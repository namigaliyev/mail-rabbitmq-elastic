using System.Threading.Tasks;
using Core.Http.Request.Mail;

namespace Service.Interfaces
{
    public interface IMailCreateAsync
    {
        Task<MailRequest> MailCreateAsync(MailRequest model);
    }
}